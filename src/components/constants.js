export const baseUrl = process.env.PUBLIC_URL;
export const apiUrl = 'http://admin-claire.tallorder.mobi';
export const paginationSize = 6; // the pagination bar size
export const rowsPerPage = 10; // which size per page you want to locate as default
export const filterActive = {0: 'Inactive',1: 'Active'};
export const tid = 18;
