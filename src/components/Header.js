import React, { Component } from 'react';
import {NavLink} from 'react-router-dom';

class Header extends Component {

  breadcrumbs() {
    console.log('this.props.breadcrumbs',this.props.breadcrumbs);
    var extraBreadcrumbs = '';
    if(this.props.breadcrumbs){
      var crumbs = this.props.breadcrumbs;
      extraBreadcrumbs = Object.keys(crumbs).map(function(key) {
        return <li key={key}><NavLink to={crumbs[key]} activeClassName="active">{key}</NavLink></li>;
      });
    }
    return (
      <ul className="breadcrumb">
        <li><NavLink to='/' exact activeClassName="active">Home</NavLink></li>
        {extraBreadcrumbs}
        <li className="active">{this.props.title}</li>
      </ul>
    );
  }

  render() {
    return (
      <div className="content-header">
        <div className="row">
          <div className="col-sm-12">
            { this.breadcrumbs() }
          </div>
        </div>
      </div>
    )
  }

}

export default Header;
