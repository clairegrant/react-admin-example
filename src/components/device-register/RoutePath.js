import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Header from '../Header.js';
import ListView from './ListView';
import DeviceRegisterForm from './Form';
import Detail from './Detail';
import {baseUrl} from '../constants.js';

const RoutePath = () => (
 <div id="page-content">
   <Switch>
  <Route exact path={baseUrl + "/"} render={(props) => (<Header title="Device Register" {...props}/>)} />
   <Route path={baseUrl + "/list-view"} render={(props) => (<Header title="Device Register" {...props}/>)}/>
   <Route path={baseUrl + "/form"} render={(props) => (<Header title="Add a new device" breadcrumbs={{'Device Register':'list-view'}} {...props}/>)} />
   <Route path={baseUrl + "/detail"} render={(props) => (<Header title="Device Details" breadcrumbs={{'Device Register':'list-view'}} {...props}/>)} />
  </Switch>
   <div id="inner-content">
     <Switch>
    <Route exact path={baseUrl + "/"} component={ListView} />
     <Route path={baseUrl + "/list-view"} component={ListView}/>
     <Route path={baseUrl + "/form"} component={DeviceRegisterForm} />
     <Route path={baseUrl + "/detail"} component={Detail} />
    </Switch>
   </div>
 </div>

)
export default RoutePath
