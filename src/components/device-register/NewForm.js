import React, { Component } from 'react';
import * as myConst from '../constants.js';
import {get} from '../../services/api.js';

var urlTypes = myConst.apiUrl+'/test/device-types';
var urlStationDevices = myConst.apiUrl+'/test/station-devices?tid='+myConst.tid

class DeviceRegisterForm extends Component {

  constructor( props ) {
      super( props );
      this.state = {
        types: [],
        stationDevices : [],
        DeviceRegister: {
          'DeviceTypeID':{'value':4},
          'Device':{'value':''},
          'DeviceUID':{'value':''},
          'DefaultPrinter':{'value':''},
          'Address':{'value':''},
          'Hostname':{'value':''},
          'DevicePIN':{'value':''},
          'AuthCode':{'value':''},
          'PushDeviceToken':{'value':''},
          'PosGPS':{'value':''},
          'Version':{'value':''},
          'Active':{'value':'1'},
          'Master':{'value':'0'},
        }
      };
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
  }

  getTypes(){
    var rows = [];
    get(urlTypes, null).then((results) => {
      // Loop over results and convert JSON data
      rows = JSON.parse(results).map((row) => {
        return <option key={row.ID} value={row.ID}>{row.DeviceInfo}</option>
     });
     this.setState({types : rows})
   });
  }

  getStationDevices(){
    var rows = [];
    get(urlStationDevices, null).then((results) => {
      // Loop over results and convert JSON data
      rows = results.map((row) => {
        return <option key={row.ID} value={row.ID}>{row.DeviceName}</option>
     });
     this.setState({stationDevices : rows})
   });
  }

  componentWillMount (){
    this.getTypes();
    this.getStationDevices();
  }

  handleChange(field,event) {
    var newValue = event.target.value;
    this.setState( ({DeviceRegister}) => ({DeviceRegister: {...DeviceRegister,[field]: {'value':newValue}}}) );
  }

  handleSubmit(event) {
    event.preventDefault();
    //Validate input client side
    //Submit form
    //Handle server returned errors
  }

  render() {
    return  (
      <div className="container-fluid">
        <form className="form-inline" onSubmit={this.handleSubmit}>
          <input type="hidden" id="deviceregister-tenantid" name="TenantID" value={myConst.tid}/>

            <div className="row push-bottom-20-sm">
              <div className="col-xs-12 col-sm-6">
                <div className="form-group">
                  <label htmlFor="deviceregister-device">Device Name</label>
                  <input type="text" id="deviceregister-device" className="full_width form-control" name="DeviceRegister[Device]" maxLength="255" value={this.state.DeviceRegister.Device.value} onChange={(e) => this.handleChange('Device', e)}/>
                </div>
              </div>
              <div className="col-xs-12 col-sm-6">
                <div className="form-group">
                  <label htmlFor="deviceregister-devicetypeid">Device Type</label>
                  <select id="deviceregister-devicetypeid" className="form-control" name="DeviceRegister[DeviceTypeID]" value={this.state.DeviceRegister.DeviceTypeID.value} onChange={(e) => this.handleChange('DeviceTypeID', e)}>
                    <option value="">Select a Device Type</option>
                    {this.state.types}
                  </select>
                </div>
              </div>
            </div>

            <div className="row push-bottom-20-sm">
              <div className="col-xs-12 col-sm-6">
                <div className="form-group">
                  <label htmlFor="deviceregister-deviceuid">Device UID</label>
                  <input type="text" id="deviceregister-deviceuid" className="full_width form-control" name="DeviceRegister[DeviceUID]" maxLength="60" aria-required="true" value={this.state.DeviceRegister.DeviceUID.value} onChange={(e) => this.handleChange('DeviceUID', e)}/>
                </div>
              </div>
              <div className="col-xs-12 col-sm-6">
                <div className="col-xs-6">
                  <div className="form-group">
                    <div className="form-group">
                      <label htmlFor="deviceregister-active">Active</label>
                      <div id="deviceregister-active">
                      <label className="radio-inline"><input type="radio" name="DeviceRegister[Active]" value="1" checked={this.state.DeviceRegister.Active.value === '1'} onChange={(e) => this.handleChange('Active', e)}/> Yes</label>
                      <label className="radio-inline"><input type="radio" name="DeviceRegister[Active]" value="0" checked={this.state.DeviceRegister.Active.value === '0'} onChange={(e) => this.handleChange('Active', e)}/> No</label>
                    </div>
                    </div>
                  </div>
                </div>
                <div className="col-xs-6">
                  <div className="form-group">
                    <label htmlFor="deviceregister-master">Master</label>
                    <div id="deviceregister-master">
                    <label className="radio-inline"><input type="radio" name="DeviceRegister[Master]" value="1" checked={this.state.DeviceRegister.Master.value === '1'} onChange={(e) => this.handleChange('Master', e)}/> Yes</label>
                    <label className="radio-inline"><input type="radio" name="DeviceRegister[Master]" value="0" checked={this.state.DeviceRegister.Master.value === '0'} onChange={(e) => this.handleChange('Master', e)}/> No</label>
                  </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="row push-bottom-20-sm">
              <div className="col-xs-12 col-sm-6">
                <div className="form-group">
                  <label htmlFor="deviceregister-address">Address</label>
                  <input type="text" id="deviceregister-address" className="full_width form-control" name="DeviceRegister[Address]" maxLength="255" value={this.state.DeviceRegister.Address.value} onChange={(e) => this.handleChange('Address', e)}/>
                </div>
              </div>
              <div className="col-xs-12 col-sm-6">
                <div className="form-group">
                  <label htmlFor="deviceregister-hostname">Hostname</label>
                  <input type="text" id="deviceregister-hostname" className="full_width form-control" name="DeviceRegister[Hostname]" maxLength="255" value={this.state.DeviceRegister.Hostname.value} onChange={(e) => this.handleChange('Hostname', e)}/>
                </div>
              </div>
            </div>

            <div className="row push-bottom-20-sm">
              <div className="col-xs-12 col-sm-6">
                <div className="form-group">
                  <label htmlFor="deviceregister-defaultprinter">Default Printer</label>
                  <select id="deviceregister-defaultprinter" className="form-control" name="DeviceRegister[DefaultPrinter]" value={this.state.DeviceRegister.DefaultPrinter.value} onChange={(e) => this.handleChange('DefaultPrinter', e)}>
                    <option value="">Select a Device</option>
                    {this.state.stationDevices}
                  </select>
                </div>
              </div>
              <div className="col-xs-12 col-sm-6">
                <div className="form-group">
                  <label htmlFor="deviceregister-devicepin">Device Pin</label>
                  <input type="text" id="deviceregister-devicepin" className="full_width form-control" name="DeviceRegister[DevicePIN]" maxLength="40" value={this.state.DeviceRegister.DevicePIN.value} onChange={(e) => this.handleChange('DevicePIN', e)}/>
                </div>
              </div>
            </div>

            <div className="row push-bottom-20-sm">
              <div className="col-xs-12 col-sm-6">
                <div className="form-group">
                  <label htmlFor="deviceregister-authcode">Auth Code</label>
                  <input type="text" id="deviceregister-authcode" className="full_width form-control" name="DeviceRegister[AuthCode]" maxLength="16" value={this.state.DeviceRegister.AuthCode.value} onChange={(e) => this.handleChange('AuthCode', e)}/>
                </div>
              </div>
              <div className="col-xs-12 col-sm-6">
                <div className="form-group">
                  <label htmlFor="deviceregister-pushdevicetoken">Push Device Token</label>
                  <input type="text" id="deviceregister-pushdevicetoken" className="full_width form-control" name="DeviceRegister[PushDeviceToken]" maxLength="64" value={this.state.DeviceRegister.PushDeviceToken.value} onChange={(e) => this.handleChange('PushDeviceToken', e)}/>
                </div>
              </div>
            </div>

            <div className="row push-bottom-20-sm">
              <div className="col-xs-12 col-sm-6">
                <div className="form-group">
                  <label htmlFor="deviceregister-posgps">POS GPS</label>
                  <input type="text" id="deviceregister-posgps" className="full_width form-control" name="DeviceRegister[PosGPS]" maxLength="255" value={this.state.DeviceRegister.PosGPS.value} onChange={(e) => this.handleChange('PosGPS', e)}/>
                </div>
              </div>
              <div className="col-xs-12 col-sm-6">
                <div className="form-group">
                  <label htmlFor="deviceregister-version">Version</label>
                  <input type="text" id="deviceregister-version" className="full_width form-control" name="DeviceRegister[Version]" maxLength="8" value={this.state.DeviceRegister.Version.value} onChange={(e) => this.handleChange('Version', e)}/>
                </div>
              </div>
            </div>

            <div className="row push-bottom-20-sm">
              <div className="col-xs-12 col-sm-6">
                <button type="submit" className="btn btn-dark">Remove</button>
              </div>
              <div className="col-xs-12 col-sm-6">
                <button type="submit" className="btn btn-primary pull-right">Create</button>
                <button type="submit" className="btn btn-plain pull-right">Cancel</button>
              </div>
            </div>

        </form>
    </div>
    );
  }

}

export default DeviceRegisterForm;
