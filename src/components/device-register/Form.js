import React, { Component } from 'react';
import * as myConst from '../constants.js';
import {get} from '../../services/api.js';

var urlTypes = myConst.apiUrl+'/test/device-types';
var urlStationDevices = myConst.apiUrl+'/test/station-devices?tid='+myConst.tid

class DeviceRegisterForm extends Component {

  constructor( props ) {
      super( props );
      this.state = {
        types: [],
        stationDevices : [],
        DeviceRegister: {
          'DeviceTypeID':{'value':4},
          'Device':{'value':''},
          'DeviceUID':{'value':''},
          'DefaultPrinter':{'value':''},
          'Address':{'value':''},
          'Hostname':{'value':''},
          'DevicePIN':{'value':''},
          'AuthCode':{'value':''},
          'PushDeviceToken':{'value':''},
          'PosGPS':{'value':''},
          'Version':{'value':''},
          'IsArchived':{'value':'0'},
          'Active':{'value':'1'},
          'Master':{'value':'0'},
        }
      };
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
  }

  getTypes(){
    var rows = [];
    get(urlTypes, null).then((results) => {
      // Loop over results and convert JSON data
      rows = JSON.parse(results).map((row) => {
        return <option key={row.ID} value={row.ID}>{row.DeviceInfo}</option>
     });
     this.setState({types : rows})
   });
  }

  getStationDevices(){
    var rows = [];
    get(urlStationDevices, null).then((results) => {
      // Loop over results and convert JSON data
      rows = results.map((row) => {
        return <option key={row.ID} value={row.ID}>{row.DeviceName}</option>
     });
     this.setState({stationDevices : rows})
   });
  }

  componentWillMount (){
    this.getTypes();
    this.getStationDevices();
  }

  handleChange(field,event) {
    var newValue = event.target.value;
    this.setState( ({DeviceRegister}) => ({DeviceRegister: {...DeviceRegister,[field]: {'value':newValue}}}) );
  }

  handleSubmit(event) {
    event.preventDefault();
    //Validate input client side
    //Submit form
    //Handle server returned errors
  }

  render() {
    return  (
      <div>
        <form className="form-horizontal" onSubmit={this.handleSubmit}>
          <input type="hidden" id="deviceregister-tenantid" name="TenantID" value={myConst.tid}/>
          <div className="form-group field-deviceregister-devicetypeid">
            <label className="control-label col-sm-4 col-md-3" htmlFor="deviceregister-devicetypeid">Device Type</label>
            <div className="col-sm-8 col-md-9">
              <select id="deviceregister-devicetypeid" className="form-control" name="DeviceRegister[DeviceTypeID]" value={this.state.DeviceRegister.DeviceTypeID.value} onChange={(e) => this.handleChange('DeviceTypeID', e)}>
                <option value="">Select a Device Type</option>
                {this.state.types}
              </select>
              <div className="help-block help-block-error "></div>
            </div>
          </div>
          <div className="form-group field-deviceregister-device">
            <label className="control-label col-sm-4 col-md-3" htmlFor="deviceregister-device">Device</label>
            <div className="col-sm-8 col-md-9">
              <input type="text" id="deviceregister-device" className="form-control" name="DeviceRegister[Device]" maxLength="255" value={this.state.DeviceRegister.Device.value} onChange={(e) => this.handleChange('Device', e)}/>
                <div className="help-block help-block-error "></div>
            </div>
          </div>
          <div className="form-group field-deviceregister-deviceuid required">
            <label className="control-label col-sm-4 col-md-3" htmlFor="deviceregister-deviceuid">Device UID</label>
              <div className="col-sm-8 col-md-9">
                <input type="text" id="deviceregister-deviceuid" className="form-control" name="DeviceRegister[DeviceUID]" maxLength="60" aria-required="true" value={this.state.DeviceRegister.DeviceUID.value} onChange={(e) => this.handleChange('DeviceUID', e)}/>
                <div className="help-block help-block-error "></div>
              </div>
          </div>
          <div className="form-group field-deviceregister-master">
            <label className="control-label col-sm-4 col-md-3">Master</label>
            <div className="col-sm-8 col-md-9">
              <div id="deviceregister-master">
              <label className="radio-inline"><input type="radio" name="DeviceRegister[Master]" value="1" checked={this.state.DeviceRegister.Master.value === '1'} onChange={(e) => this.handleChange('Master', e)}/> Yes</label>
              <label className="radio-inline"><input type="radio" name="DeviceRegister[Master]" value="0" checked={this.state.DeviceRegister.Master.value === '0'} onChange={(e) => this.handleChange('Master', e)}/> No</label>
            </div>
              <div className="help-block help-block-error "></div>
            </div>
          </div>
          <div className="form-group field-deviceregister-active">
            <label className="control-label col-sm-4 col-md-3">Active</label>
              <div className="col-sm-8 col-md-9">
                <div id="deviceregister-active">
                  <label className="radio-inline"><input type="radio" name="DeviceRegister[Active]" value="1" checked={this.state.DeviceRegister.Active.value === '1'} onChange={(e) => this.handleChange('Active', e)}/> Yes</label>
                  <label className="radio-inline"><input type="radio" name="DeviceRegister[Active]" value="0" checked={this.state.DeviceRegister.Active.value === '0'} onChange={(e) => this.handleChange('Active', e)}/> No</label></div>
                  <div className="help-block help-block-error "></div>
              </div>
          </div>
          <div className="form-group field-deviceregister-defaultprinter">
            <label className="control-label col-sm-4 col-md-3" htmlFor="deviceregister-defaultprinter">Default Printer</label>
            <div className="col-sm-8 col-md-9">
              <select id="deviceregister-defaultprinter" className="form-control" name="DeviceRegister[DefaultPrinter]" value={this.state.DeviceRegister.DefaultPrinter.value} onChange={(e) => this.handleChange('DefaultPrinter', e)}>
                <option value="">Select a Device</option>
                {this.state.stationDevices}
              </select>
              <div className="help-block help-block-error "></div>
            </div>
          </div>
          <div className="form-group field-deviceregister-address">
            <label className="control-label col-sm-4 col-md-3" htmlFor="deviceregister-address">Address</label>
            <div className="col-sm-8 col-md-9">
              <input type="text" id="deviceregister-address" className="form-control" name="DeviceRegister[Address]" maxLength="255" value={this.state.DeviceRegister.Address.value} onChange={(e) => this.handleChange('Address', e)}/>
                <div className="help-block help-block-error "></div>
            </div>
          </div>
          <div className="form-group field-deviceregister-hostname">
            <label className="control-label col-sm-4 col-md-3" htmlFor="deviceregister-hostname">Hostname</label>
            <div className="col-sm-8 col-md-9">
              <input type="text" id="deviceregister-hostname" className="form-control" name="DeviceRegister[Hostname]" maxLength="255" value={this.state.DeviceRegister.Hostname.value} onChange={(e) => this.handleChange('Hostname', e)}/>
              <div className="help-block help-block-error "></div>
            </div>
          </div>
          <div className="form-group field-deviceregister-devicepin">
            <label className="control-label col-sm-4 col-md-3" htmlFor="deviceregister-devicepin">Device Pin</label>
            <div className="col-sm-8 col-md-9">
              <input type="text" id="deviceregister-devicepin" className="form-control" name="DeviceRegister[DevicePIN]" maxLength="40" value={this.state.DeviceRegister.DevicePIN.value} onChange={(e) => this.handleChange('DevicePIN', e)}/>
                <div className="help-block help-block-error "></div>
            </div>
          </div>
          <div className="form-group field-deviceregister-authcode">
            <label className="control-label col-sm-4 col-md-3" htmlFor="deviceregister-authcode">Auth Code</label>
            <div className="col-sm-8 col-md-9">
              <input type="text" id="deviceregister-authcode" className="form-control" name="DeviceRegister[AuthCode]" maxLength="16" value={this.state.DeviceRegister.AuthCode.value} onChange={(e) => this.handleChange('AuthCode', e)}/>
                <div className="help-block help-block-error "></div>
            </div>
          </div>
          <div className="form-group field-deviceregister-pushdevicetoken">
            <label className="control-label col-sm-4 col-md-3" htmlFor="deviceregister-pushdevicetoken">Push Device Token</label>
            <div className="col-sm-8 col-md-9">
              <input type="text" id="deviceregister-pushdevicetoken" className="form-control" name="DeviceRegister[PushDeviceToken]" maxLength="64" value={this.state.DeviceRegister.PushDeviceToken.value} onChange={(e) => this.handleChange('PushDeviceToken', e)}/>
                <div className="help-block help-block-error "></div>
            </div>
          </div>
          <div className="form-group field-deviceregister-posgps">
            <label className="control-label col-sm-4 col-md-3" htmlFor="deviceregister-posgps">POS GPS</label>
            <div className="col-sm-8 col-md-9">
              <input type="text" id="deviceregister-posgps" className="form-control" name="DeviceRegister[PosGPS]" maxLength="255" value={this.state.DeviceRegister.PosGPS.value} onChange={(e) => this.handleChange('PosGPS', e)}/>
                <div className="help-block help-block-error "></div>
            </div>
          </div>
          <div className="form-group field-deviceregister-version">
            <label className="control-label col-sm-4 col-md-3" htmlFor="deviceregister-version">Version</label>
            <div className="col-sm-8 col-md-9">
              <input type="text" id="deviceregister-version" className="form-control" name="DeviceRegister[Version]" maxLength="8" value={this.state.DeviceRegister.Version.value} onChange={(e) => this.handleChange('Version', e)}/>
                <div className="help-block help-block-error "></div>
            </div>
          </div>
          <div className="form-group field-deviceregister-isarchived">
            <label className="control-label col-sm-4 col-md-3">Is Archived</label>
            <div className="col-sm-8 col-md-9">
              <div id="deviceregister-isarchived">
                <label className="radio-inline"><input type="radio" name="DeviceRegister[IsArchived]" value="1" checked={this.state.DeviceRegister.IsArchived.value === '1'} onChange={(e) => this.handleChange('IsArchived', e)}/> Yes</label>
                <label className="radio-inline"><input type="radio" name="DeviceRegister[IsArchived]" value="0" checked={this.state.DeviceRegister.IsArchived.value === '0'} onChange={(e) => this.handleChange('IsArchived', e)}/> No</label>
              </div>
              <div className="help-block help-block-error "></div>
            </div>
          </div>
          <button type="submit" className="btn btn-success"><i className="fa fa-check" aria-hidden="true"></i> Create</button>
        </form>
    </div>
    );
  }

}

export default DeviceRegisterForm;
