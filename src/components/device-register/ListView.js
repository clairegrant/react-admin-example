import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {BootstrapTable, TableHeaderColumn, InsertButton, SearchField, InsertModalHeader, InsertModalFooter} from 'react-bootstrap-table';
import '../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import * as myConst from '../constants.js';
import * as myFormat from '../data-format.js';
import {get} from '../../services/api.js';
import DeviceRegisterForm from './NewForm.js';

var url = myConst.apiUrl+'/test/device-list?tid='+myConst.tid;

function deviceNameFormatter(cell, row) {
  return (cell!==null) ? `<a href="#" class="textlink">${cell}</a>` : `<a href="#" class="textlink">Unknown</a>`;
}

class ActionFormatter extends Component  {
  render() {
    let actionArray = [];
    //Database backup
    if(Number(this.props.deviceType)===11){
      if (Number(this.props.active)===1){
        actionArray.push(<a href="#" key='db{this.props.id}'><i className="fa fa-upload text-muted action-icon" aria-hidden="true"></i></a>);
      }else{
        actionArray.push(<i key='db{this.props.id}' className="fa fa-upload action-disabled action-icon" aria-hidden="true"></i>);
      }
    }else{
      actionArray.push(<i key='db{this.props.id}' className="fa fa-upload action-disabled action-icon" aria-hidden="true"></i>);
    }
    //Database Download
    if(Number(this.props.deviceType)===11){
      //TODO: join PosDatabaseBackups then check for 'S3_URL'
      actionArray.push(<a href="#" key='dd{this.props.id}'><i className="fa fa-download text-muted action-icon push_left_20" aria-hidden="true"></i></a>);
    }else{
      actionArray.push(<i key='dd{this.props.id}' className="fa fa-download action-disabled action-icon push_left_20" aria-hidden="true"></i>);
    }
    //edit
    actionArray.push(<a href="#" key='e{this.props.id}'><i className="fa fa-pencil-square action-icon push_left_20" aria-hidden="true"></i></a>);
    //delete
    actionArray.push(<a href="#" key='d{this.props.id}'><i className="fa fa-trash action-icon text-muted push_left_20" aria-hidden="true"></i></a>);
    return(<div>{actionArray}</div>);
  }
}

function actionFormatter(cell, row) {
  return (
    <ActionFormatter deviceType={row['deviceType']} active={row['active']} id={row['id']}/>
  );
}

const selectRowProp = {
  mode: 'checkbox'
};

class ListView extends Component {

  constructor(props) {
    super(props);
    this.state = {rows: []}
  }

  getRows(){
    var rows = [];
    get(url, null).then((results) => {
      // Loop over results and convert JSON data
      rows = results.map((row) => {
        return {
          'actions': 'action test',
          'device': row.Device,
          'deviceType': row.DeviceTypeID,
          'deviceUid': row.DeviceUID,
          'version': row.Version,
          'active': row.Active,
          'master': row.Master,
          'id': row.ID
        }
     });
     this.setState({rows : rows})
   });
  }

  componentWillMount (){
    this.getRows();
  }

  createCustomToolBar = props => {
  /**
   *  http://allenfang.github.io/react-bootstrap-table/custom.html#toolbar
   **/
   return (
    <div>
      <div className="col-xs-2"><span className="toolbar-title">Device Register</span></div>
      <div className="col-xs-6 text-right">
        { props.components.insertBtn }
        <button type="button" class="btn btn-default push_left_10">
          <span><i class="fa fa-wrench" aria-hidden="true"></i> Bulk Actions</span>
        </button>
      </div>
      <div className="col-xs-4">{ props.components.searchPanel }</div>
    </div>
  );
  }

  createCustomInsertButton = (onClick) => {
  return (
    <InsertButton
      btnText='Create New Device'
      btnContextual='btn-primary'
      btnGlyphicon='glyphicon-plus-sign'/>
  );
  }

  createCustomSearchField = (props) => {
  return (
    <SearchField
        placeholder='Search Devices'/>
  );
  }

  renderShowsTotal(start, to, total) {
      return (
        <span>
          Showing { start }-{ to } of { total } items
        </span>
      );
  }

  renderPaginationPanel = (props) => {
  return (
    <div>
      <div className="col-xs-12">
        <div className="pull-left text-muted">{props.components.totalText}</div>
        <div className="pull-right push_left_10">{ props.components.pageList }</div>
        <div className="pull-right">{ props.components.sizePerPageDropdown }</div>
      </div>
    </div>
  );
}

createDeviceModalBody = (columns, validateState, ignoreEditable) => {
  return (
      <div className="col-xs-12 pad-top-20 modal_body_bg">
        <DeviceRegisterForm/>
      </div>
  );
  }

  createDeviceModalHeader = (closeModal, save) => {
  return (
    <InsertModalHeader
      title='Create Device'
      />
      //beforeClose={ this.beforeClose }
      //onModalClose={ () => this.handleModalClose(closeModal) }
      // hideClose={ true } to hide the close button
  );
}

modalFooter = () => {
  return (<div></div>);
}

  render() {
    const options = {
      page: 1,  // which page you want to show as default
      sizePerPageList: [ {text: '10', value: 10}, {text: '20', value: 20}, {text: '50', value: 50}, {text: '100', value: 100},{text: 'All', value: this.state.rows.length} ], // you can change the dropdown list for size per page
      sizePerPage: myConst.rowsPerPage,  // which size per page you want to locate as default
      pageStartIndex: 1, // where to start counting the pages
      paginationSize: myConst.paginationSize,  // the pagination bar size.
      prePage: 'Prev', // Previous page button text
      nextPage: 'Next', // Next page button text
      firstPage: 'First', // First page button text
      lastPage: 'Last', // Last page button text
      paginationShowsTotal: this.renderShowsTotal,  // Accept bool or function
      paginationPosition: 'both',  // default is bottom, top and both is all available
      // hideSizePerPage: true > You can hide the dropdown for sizePerPage
      // alwaysShowAllBtns: true // Always show next and previous button
      // withFirstAndLast: false > Hide the going to First and Last page button
      noDataText: 'No devices setup yet',
      toolBar: this.createCustomToolBar,
      insertBtn: this.createCustomInsertButton,
      searchField: this.createCustomSearchField,
      paginationPanel: this.renderPaginationPanel,
      insertModalBody: this.createDeviceModalBody,
      insertModalHeader: this.createDeviceModalHeader,
      insertModalFooter: this.modalFooter
    };

    return  (
      <div className="block">
        <div className="grid-view">
          <BootstrapTable data={this.state.rows} striped hover search pagination={ true } options={ options } bordered={ false } selectRow={ selectRowProp } insertRow>
            <TableHeaderColumn dataField='device' dataSort={ true } dataFormat={ deviceNameFormatter }>Device Name</TableHeaderColumn>
            <TableHeaderColumn dataField='deviceType' dataSort={ true }>Device Type</TableHeaderColumn>
            <TableHeaderColumn dataField='deviceUid' dataSort={ true }>Device UID</TableHeaderColumn>
            <TableHeaderColumn dataField='active' width='80' headerAlign='center' dataAlign='center' dataSort={ true } dataFormat={ myFormat.enumFormatter } formatExtraData={ myFormat.activeIcon }>Active</TableHeaderColumn>
            <TableHeaderColumn dataField='master' width='90' headerAlign='center' dataAlign='center' dataSort={ true } dataFormat={ myFormat.enumFormatter } formatExtraData={ myFormat.masterIcon }>Master</TableHeaderColumn>
            <TableHeaderColumn dataField='actions' width='190' dataFormat={actionFormatter}>Actions</TableHeaderColumn>
            <TableHeaderColumn dataField='id' isKey={ true } hidden>ID</TableHeaderColumn>
          </BootstrapTable>
      </div>

    </div>
    );
  }

}

export default ListView;
