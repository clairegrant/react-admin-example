export const filterActive = {0: 'Inactive',1: 'Active'};
export const masterIcon = {0: '', 1: '<i class="fa fa-star text-warning action-icon" aria-hidden="true"></i>'};
export const activeIcon = {0: '<i class="fa fa-times-circle-o text-danger action-icon" aria-hidden="true"></i>', 1: '<i class="fa fa-check-circle-o text-success action-icon" aria-hidden="true"></i>'};
export function enumFormatter(cell, row, enumObject) {
  return enumObject[cell];
}
