import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';

import MainContent from './components/device-register/RoutePath.js';

ReactDOM.render(
    <BrowserRouter basename='/react/device-register'>
      <MainContent/>
    </BrowserRouter>
    ,document.getElementById('root')
  );
