// use Fetch
export var get = (url, data) => (
  fetch(url, {
  body: data
  })
  .then(response => stringifyResponse(response))
);

// Auto unwrap the response as we want the data not the full headers for this simple example
export const stringifyResponse = response => {
  if (response.status >= 400) throw new Error(response);
  return response.headers.get("Content-Type").includes("application/json") ? response.json() : response.text();
};
